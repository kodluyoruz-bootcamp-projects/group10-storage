package com.trendyol.storage.controller;


import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.trendyol.storage.model.RequestFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/storage")
public class StorageController {

    @Autowired
    private Storage storage;

    @PostMapping()
    private String uploadFile(@RequestBody RequestFile file) {
        String bucketName = "medium-clone";
        try {
            BlobInfo blobInfo = storage.create(
                    BlobInfo.newBuilder(bucketName,
                            UUID.randomUUID().toString() + "." + file.getContentType()).build(),
                    file.getData(),
                    Storage.BlobTargetOption.predefinedAcl(Storage.PredefinedAcl.PUBLIC_READ)
            );
            return blobInfo.getMediaLink();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

