package com.trendyol.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@Controller
public class StorageApplication {

    @GetMapping("/")
    public String home() {
        return "redirect:/swagger-ui.html";
    }

    public static void main(String[] args) {
        SpringApplication.run(StorageApplication.class, args);
    }

}
